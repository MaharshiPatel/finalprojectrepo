# Useful OpenShift commands for support

## Getting a list of pods

```
os> oc get pod
NAME               READY     STATUS    RESTARTS   AGE
activemq-6-rcr7h   1/1       Running   0          8m
mysql-1-tqhxj      1/1       Running   2          3h
trades-6-rx9lr     1/1       Running   0          5m

```

## Getting a log from a pod

```
os> oc log <podName>
```

Where podName is the name of the pod you are interested in from ```oc get pod```

## Get list of image streams

```
os> oc get is
NAME       DOCKER REPO                         TAGS      UPDATED
activemq   172.30.1.1:5000/tradeapp/activemq   1.0.0     9 minutes ago
mysql      172.30.1.1:5000/tradeapp/mysql      1.0.0     4 hours ago
trades     172.30.1.1:5000/tradeapp/trades     1.0.0     3 hours ago
```

## Show deployed container information

```
os> oc get dc
NAME       REVISION   DESIRED   CURRENT   TRIGGERED BY
activemq   6          1         1         config,image(activemq:1.0.0)
mysql      1          1         1         config,image(mysql:1.0.0)
trades     6          1         1         config,image(trades:1.0.0)
```

Where Revision is the current deployment count, desired and current are for those that are load balanced.  Triggered by shows the image name

## Show service information

```
os> oc get svc
NAME       CLUSTER-IP       EXTERNAL-IP   PORT(S)              AGE
activemq   172.30.132.229   <none>        61616/TCP,8161/TCP   4h
mysql      172.30.18.80     <none>        3306/TCP             4h
trades     172.30.131.232   <none>        8080/TCP,7091/TCP    4h
```

EXTERNAL-IP would be the hostname and port for the application

## Deploying your Docker Compose as an OpenShift project

To do this you should create the relevant Dockerfiles to create your containers, and the docker-compose.yaml file to bring your services together with any persistent volumes, ports to map, and names to link for host names, etc.  This project contains exactly that.

Install the **kompose** application, see https://kubernetes.io/docs/tasks/configure-pod-container/translate-compose-kubernetes/

Once you know your Docker system is working as expected then you run;

```
os> kompose convert -f docker-compose.yaml --provider=openshift
```

This will generate all the relevant yaml files for importing into OpenShift, it may be worth running it in a subfolder and pointing to ../docker-compose.yaml so that you don't get confused which files are required for your OpenShift project.

Create your OpenShift project if you don't already have one;

```
os> oc new-app <yourAppName>
```

Where <yourAppName> is what you want to group you project into.  This will also set your current project.

If you already have an existing project and you want to reload any one of the yaml files then use;

```
os> oc project <yourAppName>
```

Once you have the yaml files, you can run the following to import into your project;

```
os> oc apply -f `echo *.yaml | sed 's/ /,/g'`
```

## Show status of the OpenShift server

```
os> oc status
```

## Delete a route

Get the list of routes;
```
os> oc get route
```

Use the name to delete the object;
```
os> oc delete route <routeName>
```

## Other references

* CLI basic operations
  * https://docs.openshift.com/enterprise/3.0/cli_reference/basic_cli_operations.html
* Ingress external ports
  * https://docs.okd.io/latest/admin_guide/tcp_ingress_external_ports.html
* Port forwarding
  * https://docs.okd.io/latest/minishift/openshift/exposing-services.html
